import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFire, FirebaseListObservable, AuthProviders, AuthMethods } from 'angularfire2';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  cuisines: FirebaseListObservable<any[]>;
  restaurants: Observable<any[]>;
  exists;
  displayName;
  photoUrl;
  error;

  constructor(private af: AngularFire, private http: Http) {}

  loginEmail() {
    this.af.auth.login({
      email: 'drunk.pacifist@gmail.com',
      password: 'spamspamspam'
    }, {
      method: AuthMethods.Password,
      provider: AuthProviders.Password
    })
    .then(authState => console.log("LOGIN-THEN", authState))
    .catch(error => this.error = error.message);
  }

  login() {
    this.af.auth.login({
      provider: AuthProviders.Facebook,
      method: AuthMethods.Popup,
      scope: ['public_profile', 'user_friends']
    }).then((authState: any) => {
      console.log("AFTER LOGIN", authState);
      this.af.database.object('/users/' + authState.uid).update({
        accessToken: authState.facebook.accessToken
      })
    });
  }

  logout() {
    this.af.auth.logout();
  }

  registerEmail() {
    this.af.auth.createUser({
      email: 'drunk.pacifist@gmail.com',
      password: 'spamspamspam'
    })
    .then(authState => {
      authState.auth.sendEmailVerification();
    })
    .catch(error => this.error = error.message);
  }

  ngOnInit() {
    this.af.auth.subscribe(authState => {
      if (!authState) {
        console.log("NOT LOGGED IN");
        this.displayName = null;
        this.photoUrl = null;
      } else {
        console.log("LOGGED IN");
        if (authState.facebook) {          
          let userRef = this.af.database.object('/users/' + authState.uid);
          userRef.subscribe(user => {
            let url = `https://graph.facebook.com/v2.8/${authState.facebook.uid}?fields=first_name,last_name&access_token=${user.accessToken}`;        
            this.http.get(url).subscribe(response => {
              let user = response.json();
              userRef.update({
                firstName: user.first_name,
                lastName: user.last_name  
              });
            });
          });
          this.displayName = authState.auth.displayName;
          this.photoUrl = authState.auth.photoURL;
        }
      }
    });

    this.cuisines = this.af.database.list('/cuisines', {
      query: {
        orderByKey: true
      }
    });
    this.restaurants = this.af.database.list('/restaurants', {
      query: {
        orderByChild: 'rating' 
      }
    })
      .map(restaurants => {
        restaurants.map(restaurant => {
          restaurant.cuisineType = this.af.database.object('/cuisines/' + restaurant.cuisine);
        
          restaurant.featureTypes = [];
          for (let f in restaurant.features)
            restaurant.featureTypes.push(this.af.database.object('/features/' + f));
        });
        return restaurants;   
      });

    // Check for remote object's existence
    this.exists = this.af.database.object('/restaurants/1/features/1');
    // Drop subscription after the first time we receive a response
    this.exists.take(1).subscribe(x => {
      if (x && x.$value) console.log("EXISTS");
      else console.log("NOT EXISTS");
    });
  }

  add() {
    this.cuisines.push({
      name: 'Asian'
    });
  }

  update() {
    this.af.database.object('/restaurant').update({
      name: 'New Name',
      rating: 5
    });
  }

  remove() {
    this.af.database.object('/restaurant').remove()
      .then(x => console.log("Removed successfully"))
      .catch(error => console.log("Error", error));
  }
}
