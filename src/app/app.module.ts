import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';

export const firebaseConfig = {
  apiKey: "AIzaSyAIGvGejaM-CSfqJLnSwzJmECV2r7DfabM",
  authDomain: "pepper-16fbd.firebaseapp.com",
  databaseURL: "https://pepper-16fbd.firebaseio.com",
  storageBucket: "pepper-16fbd.appspot.com",
  messagingSenderId: "575852248128" 
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
